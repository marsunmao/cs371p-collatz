// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

int cache [1000000];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;
    assert(i > 0);
    assert(j > 0);
    assert(i < 1000000);
    assert(j < 1000000);
    // <your code>
    int tempI;
    int tempJ;
    if(i < j) {
       tempI = i;
       tempJ = j;
    } else {
       tempI = j;
       tempJ = i;
    }
    assert(tempI <= tempJ);
    
    int m = tempJ / 2 + 1;
    /*
    if(m > tempI) {
      tempI = m;
    }
    */
    
    tempI = max(tempI, m);
    //assert(tempI == 210001);
    
    int mcl = 0;
    for (int n = tempI; n <= tempJ; n++) {
       int length;
       
       assert(n >= 0);
       assert(n < 1000000);
       if(cache[n] > 0) {
          length = cache[n];
       } else {
          length = 1;
          long temp = n;
          while (temp > 1) {
             if(temp % 2 == 0) {
               temp /= 2;
               length++;
             } else {
               temp = temp + temp / 2 + 1;
               length += 2;
             }
             assert(temp >= 1);
          }
         assert(length > 0);
       
         assert(n >= 0);
         assert(n < 1000000);
         cache[n] = length;
       }
       if (length > mcl) {
         mcl = length;
       }
    }
    
    assert(mcl > 0);
    return make_tuple(i, j, mcl);}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
         collatz_print(sout, collatz_eval(collatz_read(s)));}
