// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read0) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

TEST(CollatzFixture, read1) {
    ASSERT_EQ(collatz_read("10 1\n"), make_pair(10, 1));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(10, 1)), make_tuple(10, 1, 20));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1)), make_tuple(1, 1, 1));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(1, 115000)), make_tuple(1, 115000, 354));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(1, 420000)), make_tuple(1, 420000, 449));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(364156, 642887)), make_tuple(364156, 642887, 509));
}

TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(10, 7)), make_tuple(10, 7, 20));
}

TEST(CollatzFixture, eval10) {
    ASSERT_EQ(collatz_eval(make_pair(1, 2)), make_tuple(1, 2, 2));
}

TEST(CollatzFixture, eval11) {
    ASSERT_EQ(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 525));
}

TEST(CollatzFixture, eval12) {
    ASSERT_EQ(collatz_eval(make_pair(999998, 999999)), make_tuple(999998, 999999, 259));
}

TEST(CollatzFixture, eval13) {
    ASSERT_EQ(collatz_eval(make_pair(10, 10)), make_tuple(10, 10, 7));
}

TEST(CollatzFixture, eval14) {
    ASSERT_EQ(collatz_eval(make_pair(64, 64)), make_tuple(64, 64, 7));
}

TEST(CollatzFixture, eval15) {
    ASSERT_EQ(collatz_eval(make_pair(1000, 900000)), make_tuple(1000, 900000, 525));
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
